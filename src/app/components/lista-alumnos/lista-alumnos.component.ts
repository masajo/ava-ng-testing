import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlumnosService } from 'src/app/services/alumnos.service';

@Component({
  selector: 'app-lista-alumnos',
  templateUrl: './lista-alumnos.component.html',
  styleUrls: ['./lista-alumnos.component.css']
})
export class ListaAlumnosComponent implements OnInit {

  @Input() usuario: string = '';
  @Output() mensaje: EventEmitter<string> = new EventEmitter<string>();
  mensajeNuevo: string = 'Mensaje para el padre';

  alumnos: any[] = [];
  alumnoSeleccionado = {
    first_name: '',
    last_name: '',
    email: '',
    avatar: ''
  }

  constructor(public service: AlumnosService) { }

  ngOnInit(): void {
    this.service.getAlumnos().subscribe(
      (respuesta) => {
        this.alumnos = respuesta.data;
      },
      (error) => alert(`Ha habido un error al obtener los alumnos: ${error}`),
      () => console.log('Alumnos obtenidos correctamente')
    )
  }

  seleccionarAlumno(id: number){
    this.service.getAlumno(id).subscribe(
      (respuesta) => {
        this.alumnoSeleccionado = respuesta.data
      },
      (error) => alert(`ha habido un error al obtener el detalle del alumno: ${error}`),
      () => console.log('Alumno obtenido con éxito')
    )
  }

  mandarMensaje(){
    if(this.mensajeNuevo) {
      this.mensaje.emit(this.mensajeNuevo);
      this.mensajeNuevo = '';
    }
  }

}
