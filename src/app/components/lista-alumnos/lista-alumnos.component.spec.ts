import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

// Módulos necesarios del componente
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Imports propios
import { AlumnosService } from 'src/app/services/alumnos.service';
import { AlumnosServiceStub } from 'src/app/services/mocks/alumnos.service.mock';
import { ListaAlumnosComponent } from './lista-alumnos.component';

describe('ListaAlumnosComponent', () => {
  let component: ListaAlumnosComponent;
  let fixture: ComponentFixture<ListaAlumnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaAlumnosComponent],
      imports: [
        HttpClientModule,
        FormsModule
      ],
      providers: [
        // * IMPORTANTE: Le decimos que donde el componente
        // haga uso de AlumnosService, realemente use AlumnosServiceStub
        {
          provide: AlumnosService,
          useClass: AlumnosServiceStub
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  // * 1. Se debería llamar a getAlumnos en el ngOnInit del componente
  it('Se debe llamar a getAlumnos del servicio en ngOnInit del componente', () => {

    // Declaramos un espía del método getAlumnos para luego comprobar que se ha llamado
    // cuando se haya completado el ngOnInit
    spyOn(component.service, 'getAlumnos').and.callThrough();

    // Forzamos la llamada al ngOnInit del componente
    component.ngOnInit();

    // Verificamos que se haya llamado a service.getAlumnos()
    expect(component.service.getAlumnos).toHaveBeenCalledTimes(1);

  });

  // * 2. Debería guardar la lista de alumnos en una propiedad llamada alumnos
  it('Debería obtener del servicio la lista de alumnos y guardarla', () => {

    fixture.whenStable().then(() => {
      // Cuando el componente ya está estable (ha pasado ya por ngOnInit)
      // Verificamos que la lusta de alumnos no está vacía
      expect(component.alumnos.length).not.toBe(0);
    });

  });

  // * 3. Shallow testing: Deberíamos pintar la lista de alumnos
  it('Debería renderizar la lista de alumnos en el HTML', () => {

    fixture.whenStable().then(() => {

      // Nos vamos aasegurar de que la vista se actualiza
      fixture.detectChanges();

      // Buscamos el elemento en el DOM
      /**
       * <div class="alumnos">
       * ... nodos hijos con un elemento por hijo (childnodes)
       * </div>
       */
      const elemento = fixture.debugElement.query(By.css('.alumnos')).nativeElement;
      // Comprobamos que tenga un elemento dentro
      expect(elemento.childNodes[1]).not.toBeNull;
    });

  });


  // * 4. Debería poder recibir un Usuario como @Input y se debería rederizar un saludo
  it('Debería rederizar un saludo al usuario pasado por @Input', () => {

    // recibe el @Input
    component.usuario = 'Martín';
    fixture.detectChanges();
    const elemento = fixture.debugElement.nativeElement;

    expect(elemento.querySelector('#saludo').textContent.trim()).toBe('Hola, Martín');

  });


  // * 5. Debería emitir un mensaje a travé sde EventEmitter y @Output
  it('Debería emitir un mensaje a través de un evento @Output', async () => {

    spyOn(component.mensaje, 'emit');

    // Obtenemos el botón que se encarga de emitir el mensaje
    const boton = fixture.nativeElement.querySelector('#boton-emitir');

    // Obtenemos el input de texto donde se escribe el mensaje a emitir y el valor escrito
    fixture.nativeElement.querySelector('#input-mensaje').value = 'Mensaje para el padre';
    const nuevoMensaje = await fixture.nativeElement.querySelector('#input-mensaje').value;

    // Pulsamos el botón
    await boton.click();
    fixture.detectChanges();

    // Comprobamos que se haya llamado al método emit del EventEmitter con el nuevoMensaje
    expect(component.mensaje.emit).toHaveBeenCalledWith(nuevoMensaje);

  });


  describe('Comportamientos del alumno seleccionado de la lista', () => {

    it('Inicialmente, los detalles del alumno seleccionado están vacíos y se muestra un mensaje', () => {

      // Verificamos que no existe el detalle
      const alumnoDetalle = fixture.debugElement.nativeElement.querySelector('#detalle');
      expect(alumnoDetalle).toBeNull();

      // Verificamos que se muestra el mensaje
      const mensaje = fixture.debugElement.nativeElement.querySelector('#mensaje');
      expect(mensaje.innerHTML.trim()).toBe('Por favor, selecciona un alumno');

    });


    it('Al hacer click en un alumno, se muestran los detalles del alumno', () => {

      fixture.whenStable().then(() => {

        fixture.detectChanges();

        // Obtenemos el alumno que vamos a seleccionar
        const alumnoSeleccionado = fixture.debugElement.nativeElement.querySelector('#alumno-1');

        // Pulsamos sobre el alumno que implica un cambio en el HTML
        alumnoSeleccionado.click();

        fixture.whenStable().then(() => {

          fixture.detectChanges();

          const alumnoDetalle = fixture.debugElement.nativeElement.queySelector('#detalle');


          // Esperamos que tengamos el nombre en la propiedad alumno seleccionado
          expect(component.alumnoSeleccionado.first_name).toBe('George');
          // Esperamos que en el HTML se muestre el nombre y el apellido
          expect(alumnoDetalle.innerHTML.trim()).toBe('George Bluth');

        })


      })

    });






  })







});
