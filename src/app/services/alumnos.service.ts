import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  constructor(private _http: HttpClient) { }

  getAlumnos(): Observable<any> {
    return this._http.get('https://reqres.in/api/users');
  }

  getAlumno(id: number): Observable<any> {
    return this._http.get(`https://reqres.in/api/users/${id}`);
  }
}
