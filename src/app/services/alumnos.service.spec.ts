import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AlumnosService } from './alumnos.service';

/**
 *
 * AlumnosService es un servicio que realiza peticiones HTTP
 * para obtener una lista de alumnos desde
 * https://reqres.in/api/users
 *
 * Dado que las pruebas unitarias y de integración son simulaciones
 * de comportamiento para comprobar que nuestro SUT en cada caso
 * realiza lo se espera de él, debemos simular la API Rest de reqres.in
 *
 * Debemos asegurar que AlumnosService funcione correctamente, independendientemente
 * de que la API pueda fallar.
 *
 * A estas simulaciones se les llama:
 *
 * - Faked data: Datos falsos
 * - Stubs: Comportamientos simulados y fijos.
 * - Mock: Generación de Stubs a demanda. Cambios de comportamientos fijos en situaciones distintas
 *
 */

// Creamos una lista Faked Data de datos de alumnos.
const stubAlumnos = [
  {
    id: 1,
    email: 'george.bluth@reqres.in',
    first_name: 'George',
    last_name: 'Bluth',
    avatar: 'https://reqres.in/img/faces/1-image.jpg',
  },
  {
    id: 2,
    email: 'janet.weaver@reqres.in',
    first_name: 'Janet',
    last_name: 'Weaver',
    avatar: 'https://reqres.in/img/faces/2-image.jpg',
  },
  {
    id: 3,
    email: 'emma.wong@reqres.in',
    first_name: 'Emma',
    last_name: 'Wong',
    avatar: 'https://reqres.in/img/faces/3-image.jpg',
  },
  {
    id: 4,
    email: 'eve.holt@reqres.in',
    first_name: 'Eve',
    last_name: 'Holt',
    avatar: 'https://reqres.in/img/faces/4-image.jpg',
  },
  {
    id: 5,
    email: 'charles.morris@reqres.in',
    first_name: 'Charles',
    last_name: 'Morris',
    avatar: 'https://reqres.in/img/faces/5-image.jpg',
  },
  {
    id: 6,
    email: 'tracey.ramos@reqres.in',
    first_name: 'Tracey',
    last_name: 'Ramos',
    avatar: 'https://reqres.in/img/faces/6-image.jpg',
  },
];


/**
 *
 * Cuando hagamos petición a https://reqres.in/users/{id}
 * Simulamos que devuelve un alumno concreto
 */

const stubAlumno = {
  id: 1,
  email: 'george.bluth@reqres.in',
  first_name: 'George',
  last_name: 'Bluth',
  avatar: 'https://reqres.in/img/faces/1-image.jpg',
}


describe('AlumnosService', () => {

  // Declaramos las variables que vamos a usar:
  let testBed: TestBed;
  let httpMock: HttpTestingController;
  let service: AlumnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Tenemos que configurar el módulo para que funcione
      imports: [HttpClientTestingModule],
      providers: [AlumnosService]
    });

    // Inicializamos las variables que vamos a emplear en las pruebas
    testBed = getTestBed();
    httpMock = testBed.inject(HttpTestingController);
    service = testBed.inject(AlumnosService);

  });

  // * Después de cada test, quiero que se verifique el httpMock
  // para asegurarme de que se ha usado
  afterEach(() => {
    httpMock.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  // * 1. Test para obtener la lista de alumnos
  it('Debería devolver una lista de alumnos', () => {

    // El servicio debería contar con un método
    // con nombre getAlumnos() que devuelva una lista de alumnos

    // * 1. Realizamos la llamada a getAlumnos()
    service.getAlumnos().subscribe(
      (respuesta) => {
        // * 4. El flush del httpMock emite una respuesta y la capturamos
        expect(respuesta).toBe(stubAlumnos)
      },
    )

    // * 2. Cuando en el servicio, se ejecuta getAlumnos
    // se tiene que haber realizado una llamada http al endpoint
    const peticion = httpMock.expectOne('https://reqres.in/api/users');

    // * 3. El httpMock emite la lista de alumnos de tipo STUB
    peticion.flush(stubAlumnos);

    // * 5 Esperamos que la petición de http, haya sido de tipo GET
    // Esto indica que en AlumnosService, se ha realizado un GET al Endpoint del punto 2
    expect(peticion.request.method).toBe('GET');

  });

  // * 2. Test para obtener un alumno por ID
  it('Debería obtener un alumno si el ID existe', () => {

    let idAlumno: number = 1;

    service.getAlumno(idAlumno).subscribe(
      (respuesta) => {
        // * 4. El flush del httpMock emite una respuesta y la capturamos
        expect(respuesta).toBe(stubAlumno);
      }
    );

    // * 2. Cuando en el servicio, se ejecuta getAlumno(id)
    // se tiene que haber realizado una llamada http al endpoint
    const peticion = httpMock.expectOne(`https://reqres.in/api/users/${idAlumno}`);

    // * 3. El httpMock emite el alumno de tipo STUB
    peticion.flush(stubAlumno);

    // * 5 Esperamos que la petición de http, haya sido de tipo GET
    // Esto indica que en AlumnosService, se ha realizado un GET al Endpoint del punto 2
    expect(peticion.request.method).toBe('GET');

  });




});
