import { HttpClientModule } from '@angular/common/http';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ]
    }).compileComponents();

    // Antes de cada test, replanteamos el compoennte y su instancia "app"
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // R I T E --> Readable Isolated Through Explicit
  // Las pruebas deben tener: contexto - acciones - verificaciones
  // SUT --> System Under Tests (la unidad que se está probando)
  // Las pruebas tienen que estar enfocadas únicamente sobre lo que se está probando
  // En caso de que lo que se está probando tenga dependencias --> Hay que simularlas (mock, stubs, faked data, etc.)
  // El objetivo primordial es verificar que lo desarrollado es correcto, independientemente de las
  // dependencias y lo bien o mal que estén estas.
  // TDD --> Metodología de Test Driven Development
  // Fase 1: Definición de pruebas que fallan (RED)
  // Fase 2: Implementación de código que hacen pasar las pruebas (GREEN)
  // Fase 3: Refactorización del código y de las pruebas (REFACTOR)
  // Es ciclo que no termina. Fase 1 --> Fase 2 --> Fase 3 --> Fase 1 --> etc.

  it('should create the app', () => {
    expect(app).toBeTruthy(); // que no sea false, no sea null, no sea undefined y no sea 0
    // toBeTruthy != toBeTrue
    // toBeFalsy != toBeFalse
  });

  it(`should have as title 'ava-ng-testing'`, () => {
    expect(app.title).toEqual('ava-ng-testing'); // que la propiedad title de AppComponent sea ese texto
  });

  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('ava-ng-testing app is running!');
  });
});
